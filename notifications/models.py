from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Client(models.Model):
    phone_number = PhoneNumberField(region='RU')
    mobile_operator_code = models.CharField(max_length=2)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255)


class Notification(models.Model):
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    created_datetime = models.DateTimeField(auto_now_add=True)
    message_text = models.TextField()
    filter_mobile_operator_code = models.CharField(max_length=2)
    filter_tag = models.CharField(max_length=255)

    def save(self, *args, **kwargs):
        if self.end_datetime <= self.start_datetime and self.end_datetime <= self.created_datetime:
            raise ValueError("End datetime must be greater than start and created datetime.")
        super().save(*args, **kwargs)


class Message(models.Model):
    STATUS_CHOICES = [
        ('created', 'Created'),
        ('pending', 'Pending'),
        ('sent', 'Sent'),
        ('delivered', 'Delivered'),
        ('read', 'Read'),
        ('failed', 'Failed'),
        ('cancelled', 'Cancelled'),
        ('archived', 'Archived'),
    ]

    created_datetime = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=9, choices=STATUS_CHOICES, default='created')
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE)
    client = models.OneToOneField(Client, on_delete=models.CASCADE)
