from django.contrib import admin
from .models import Client, Notification, Message


class MessageInline(admin.TabularInline):
    model = Message
    extra = 1


class NotificationAdmin(admin.ModelAdmin):
    inlines = [MessageInline]
    list_display = ('start_datetime', 'end_datetime', 'created_datetime', 'filter_mobile_operator_code', 'filter_tag')


admin.site.register(Client)
admin.site.register(Message)
admin.site.register(Notification, NotificationAdmin)
