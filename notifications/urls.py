from django.urls import path
from .views import ClientListCreateAPIView, ClientDetailAPIView, NotificationListCreateAPIView, \
    NotificationDetailAPIView, MessageListCreateAPIView, MessageDetailAPIView, MessageStatisticsAPIView, \
    MessagesStatisticsAPIView, index

urlpatterns = [
    path('clients/', ClientListCreateAPIView.as_view(), name='client-list-create'),
    path('clients/<int:pk>/', ClientDetailAPIView.as_view(), name='client-detail'),

    path('notifications/', NotificationListCreateAPIView.as_view(), name='notification-list-create'),
    path('notifications/<int:pk>/', NotificationDetailAPIView.as_view(), name='notification-detail'),

    path('messages/', MessageListCreateAPIView.as_view(), name='message-list-create'),
    path('messages/<int:pk>/', MessageDetailAPIView.as_view(), name='message-detail'),

    path('messages/statistics/', MessagesStatisticsAPIView.as_view(), name='message-statistics'),
    path('messages/statistics/<int:pk>', MessageStatisticsAPIView.as_view(), name='message-statistics'),

    path('', index, name='home'),
]
