from django.db.models import Max, Min
from django.shortcuts import render
from rest_framework import generics
from rest_framework import views
from rest_framework.response import Response
from .models import Client, Notification, Message
from .serializers import ClientSerializer, NotificationSerializer, MessageSerializer
from notifications.tasks import schedule_send_notification


class ClientListCreateAPIView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class NotificationListCreateAPIView(generics.ListCreateAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer


class NotificationDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer


class MessageListCreateAPIView(generics.ListCreateAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def perform_create(self, serializer):
        instance = serializer.save()

        notification_start_time = instance.notification.start_datetime
        msg_pk = instance.pk

        schedule_send_notification.delay(notification_start_time, msg_pk)


class MessageDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer


class MessagesStatisticsAPIView(views.APIView):

    def get(self, request, *args, **kwargs):
        from django.db.models import Count
        from django.utils import timezone

        all_messages_stats = {
            'total messages': Message.objects.count(),
            'message status counts': list(Message.objects.values('status').annotate(count=Count('status'))),
            'message count per notification': list(
                Message.objects.values('notification__message_text').annotate(count=Count('notification__id'))),
            'earliest message created datetime': Message.objects.aggregate(earliest=Min('created_datetime'))[
                'earliest'],
            'latest message created datetime': Message.objects.aggregate(latest=Max('created_datetime'))['latest'],
            'upcoming messages': [msg['notification__message_text'] for msg in
                                  Message.objects.values('notification__message_text').filter(
                                      notification__start_datetime__lt=timezone.now())],
            'last past message': list(Message.objects.values('notification__message_text').filter(
                notification__end_datetime__lt=timezone.now()).last()),
        }

        return Response(all_messages_stats)


class MessageStatisticsAPIView(generics.RetrieveAPIView):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

    def retrieve(self, request, *args, **kwargs):
        message = self.get_object()

        message_statistics = {
            'status': message.status,
            'creation_time': message.created_datetime,
            'notification_info': {
                'text': message.notification.message_text,
                'filter_mobile_operator_code': message.notification.filter_mobile_operator_code,
                'filter_tag': message.notification.filter_tag,
            },
            'client_info': {
                'phone_number': str(message.client.phone_number),
                'mobile_operator_code': message.client.mobile_operator_code,
                'timezone': message.client.timezone,
                'tag': message.client.tag,
            },
        }

        return Response(message_statistics)


def index(request):
    return render(request, 'index.html', {})
