from celery import shared_task
from rest_framework.exceptions import APIException
from notifications.models import Message
from django.utils import timezone
from datetime import timedelta
import requests
import time
import os
import logging

MAX_RETRIES = 3


def construct_probe_api_request(instance):
    probe_api_url = f'https://probe.fbrq.cloud/v1/send/{instance.id}'
    probe_api_headers = {
        'Accept': 'application/json',
        'Authorization': f'Bearer {os.environ.get("PROBE_API_TOKEN", "")}',
        'Content-Type': 'application/json',
    }
    probe_api_data = {
        'id': instance.id,
        'phone': instance.client.phone_number.national_number,
        'text': instance.notification.message_text,
    }
    return probe_api_url, probe_api_headers, probe_api_data


def handle_probe_api_response(response, start_time):
    elapsed_time = timezone.now() - start_time
    response.raise_for_status()

    if elapsed_time > timedelta(seconds=1):
        logging.warning('Elapsed time exceeded 1 second')

    return {
        'status_code': response.status_code,
        'headers': dict(response.headers),
        'content': response.text,
        'elapsed_time': elapsed_time.total_seconds(),
    }


def perform_probe_api_request(instance):
    url, headers, data = construct_probe_api_request(instance)

    for attempt in range(MAX_RETRIES + 1):
        try:
            start_time = timezone.now()
            response = requests.post(url, headers=headers, json=data)
            return handle_probe_api_response(response, start_time)
        except requests.RequestException as e:
            delay = 2 ** attempt
            logging.error(f'API request failed (attempt {attempt + 1}): {e}. Retrying in {delay} seconds...')
            time.sleep(delay)

    raise APIException(detail='Max retries exceeded for API request')


@shared_task(name='api_send_notification')
def send_notification(msg_pk):
    msg = Message.objects.get(pk=msg_pk)
    probe_api_response = perform_probe_api_request(msg)

    return probe_api_response


@shared_task(name='api_schedule_send_notification')
def schedule_send_notification(specific_time, msg_pk):
    send_notification.apply_async(eta=specific_time, args=[msg_pk])
