# Notification Service

## Overview
This is a notification service for locally spin up.\
Follow the steps below to set up and run the project locally.

## Prerequisites
- Docker: [Install Docker](https://docs.docker.com/get-docker/)
- Docker Compose: [Install Docker Compose](https://docs.docker.com/compose/install/)

## Getting Started

### 1. Clone the Repository
```bash
git clone https://gitlab.com/AirGuy-one/notification_service
cd notification_service
```

### 2. Set Environment Variables
Create a .env file in the project root and set your Django project's secret key and other sensitive information. You can use the .env.example file as a template.

### 3. Build and Run Docker Containers
```bash
docker-compose build
docker-compose up
```
This will start the Django development server and other necessary services. Visit http://0.0.0.0:8000/api in your browser to access the Notification Service.

### 4. Create Superuser

To create a superuser for the Django admin interface, open a new terminal window and run the following command:

```bash
docker-compose exec web python manage.py createsuperuser
```

Follow the prompts to enter the username, email, and password for the superuser account.

### 5. Access Django Admin

Visit http://0.0.0.0:8000/admin in your browser and log in using the superuser credentials created in the previous step. Here, you can manage notifications, clients, and other relevant models.

### 6. Exploring Features
- Index Page:

Navigate to the index page where a task returning a message will be executed.
- Celery Task Monitoring:
  - Monitor all executed tasks via Celery using Flower.
  - Access Flower at http://0.0.0.0:5555.
- Statistics Information:
  - Retrieve overall statistics: http://0.0.0.0:8000/api/messages/statistics
  - Retrieve statistics for a specific notification: http://0.0.0.0:8000/api/messages/statistics/{message_id}.
- Main Website Page:
  - Access the main website page at http://0.0.0.0:8000/api.
- Swagger UI for API:
  - Explore and interact with the API using the Swagger UI.
  - Access Swagger UI at [http://0.0.0.0:8000/docs/](http://0.0.0.0:8000/docs/).


# API Endpoints

## Client Endpoints

### `GET /api/clients/`

Retrieve a list of all clients.

### `POST /api/clients/`

Create a new client.

### `GET /api/clients/{id}/`

Retrieve details of a specific client by ID.

### `PUT /api/clients/{id}/`

Update details of a specific client by ID.

### `DELETE /api/clients/{id}/`

Delete a specific client by ID.

## Notification Endpoints

### `GET /api/notifications/`

Retrieve a list of all notifications.

### `POST /api/notifications/`

Create a new notification.

### `GET /api/notifications/{id}/`

Retrieve details of a specific notification by ID.

### `PUT /api/notifications/{id}/`

Update details of a specific notification by ID.

### `DELETE /api/notifications/{id}/`

Delete a specific notification by ID.

## Message Endpoints

### `GET /api/messages/`

Retrieve a list of all messages.

### `POST /api/messages/`

Create a new message. This will also schedule a notification to be sent.

### `GET /api/messages/{id}/`

Retrieve details of a specific message by ID.

### `PUT /api/messages/{id}/`

Update details of a specific message by ID.

### `DELETE /api/messages/{id}/`

Delete a specific message by ID.

## Custom Statistics Endpoints

### `GET /api/messages/statistics/`

Retrieve various statistics related to messages, such as total message count, message status counts, message count per notification, earliest and latest message creation datetime, upcoming messages, and the last past message.

### `GET /api/messages/{id}/statistics/`

Retrieve detailed statistics for a specific message, including status, creation time, notification info, and client info.

# API Usage

- Use the appropriate HTTP methods (GET, POST, PUT, DELETE) to interact with the API endpoints.
- Ensure that you have the necessary permissions to perform CRUD operations.
- For scheduling notifications, create a new message through the `POST /api/messages/` endpoint, and the `schedule_send_notification` task will handle the rest.
