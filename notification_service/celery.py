from __future__ import absolute_import, unicode_literals
import os
import time

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'notification_service.settings')
app = Celery('notification_service')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


#@app.task(name='api_send_notification')
#def send_notification():
#    time.sleep(3)
#    task_internal_msg = 'Hello from shared task...'
#    return task_internal_msg
